<?php
namespace TYPO3\Surf;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.Surf".                 *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Base Surf exception
 */
class Exception extends \TYPO3\FLOW3\Exception {

}
?>