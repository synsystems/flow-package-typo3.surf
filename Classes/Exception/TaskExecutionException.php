<?php
namespace TYPO3\Surf\Exception;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.Surf".                 *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * An exception during task execution
 *
 * Something went wrong or an assertion during task execution was not successful.
 */
class TaskExecutionException extends \TYPO3\Surf\Exception {

}
?>