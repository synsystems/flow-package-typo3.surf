<?php
namespace TYPO3\Surf\Exception;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.Surf".                 *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Invalid deployment configuration exception
 */
class InvalidConfigurationException extends \TYPO3\Surf\Exception {

}
?>