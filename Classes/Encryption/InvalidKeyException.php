<?php
namespace TYPO3\Surf\Encryption;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.Surf".                 *
 *                                                                        *
 *                                                                        */

/**
 * Invalid key exception
 */
class InvalidKeyException extends \TYPO3\FLOW3\Exception {

}
?>